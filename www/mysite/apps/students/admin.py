from django.contrib import admin

from .models import Student
# Register your models here.

@admin.register(Student)
class StudentsAdmin(admin.ModelAdmin):
    list_display = ('name', 'sex', 'is_deleted')
    list_per_page = 50