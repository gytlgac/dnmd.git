from django.db import models


from utils.models import BaseModel
# Create your models here


class Student(BaseModel):
    SEX_CHOICES = (
        (0, '男'),
        (1, '女')
    )

    name = models.CharField(max_length=30, null=False, verbose_name='姓名')
    sex = models.IntegerField(choices=SEX_CHOICES, default=0, verbose_name='姓别')
